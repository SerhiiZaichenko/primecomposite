using NUnit.Framework;
using System;
using System.IO;

namespace PrimeComposite.Tests
{
    class PrimeCompositeTests
    {
        private TestsResultFactory _resultsFactory;
        [SetUp]
        public void Setup()
        {
            _resultsFactory = new TestsResultFactory();
        }

        [Test]
        public void PrimeComposite_PrintNumbers_PrintsNumbers()
        {
            //Arrange
            var primeComposite = new PrimeComposite();
            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);

                //Act
                primeComposite.PrintNumbers();
                var result = sw.ToString().Trim();

                //Assert
                Assert.AreEqual(_resultsFactory.GetResults(), result);
            }
        }
    }
}