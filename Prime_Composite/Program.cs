﻿using System;

namespace PrimeComposite
{
    class Program
    {
        static void Main()
        {
            var a = new PrimeComposite();
            a.PrintNumbers();
        }
    }
}
