﻿using System;
namespace PrimeComposite
{
    public class PrimeComposite
    {
        public void PrintNumbers()
        {
            for (int i = 1; i <= 100; i++)
            {
                    Console.Write(i + " ");
            }
        }
    }
}
